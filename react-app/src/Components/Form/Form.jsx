import {useState} from 'react';

const Form = () => {
  const [name, setName] = useState({text: localStorage.getItem('name') ?? '', errorText: ''});
  const [rating, setRating] = useState({text: localStorage.getItem('rating') ?? '', errorText: ''});
  const [review, setReview] = useState(localStorage.getItem('review') ?? '');

  const handleSubmit = (event) => {
    event.preventDefault();

    if (name.text.length === 0) {
        setName(prevName => ({...prevName, errorText: 'Вы забыли указать имя и фамилию'}))
        return;
    }
    if (name.text.length < 2) {
        setName(prevName => ({...prevName, errorText: 'Имя не может быть короче 2-х символов'}))
        return;
    }
    setName(prevName => ({...prevName, errorText: ''}));

    if (+rating.text < 1 || +rating.text > 5 || rating.text.length === 0) {
      setRating(prevRating => ({...prevRating, errorText: 'Оценка должна быть от 1 до 5'}))
      return;
    }
    setRating(prevRating => ({...prevRating, errorText: ''}));

    localStorage.removeItem('name');
    setName(prevName => ({...prevName, text: ''}));
    localStorage.removeItem('rating');
    setRating(prevRating => ({...prevRating, text: ''}));
    localStorage.removeItem('review');
    setReview('');
  }

  const changeNameText = (event) => {
    setName(prevName => ({...prevName, text: event.target.value}));
    localStorage.setItem('name', event.target.value);
  }
  const changeRatingText = (event) => {
    setRating(prevRating => ({...prevRating, text: event.target.value}));
    localStorage.setItem('rating', event.target.value);
  }
  const changeReviewText = (event) => {
    setReview(event.target.value);
    localStorage.setItem('review', event.target.value);
  }
  const nameFocus = () => {
    setName(prevName => ({...prevName, errorText: ''}))
  }
  const ratingFocus = () => {
    setRating(prevRating => ({...prevRating, errorText: ''}))
  }

  const nameClasses = `${name.errorText ? 'form-user-fault' : ''}`
  const ratingClasses = `${rating.errorText ? 'form-rating-fault' : ''}`
    return (

        <div className="form-block">
            <h3 className="titles">Добавить свой отзыв</h3>
                <form onSubmit={handleSubmit}>
                    <fieldset className="fieldset">

                        <div className="review-info">
                            <div>
                                <input onFocus={nameFocus} onChange={changeNameText} className="form-user" name="user" type="text" placeholder="Имя и Фамилия" value={name.text}/>
                                {name.errorText && <div className={nameClasses}>{name.errorText}</div>}
                            </div>

                            <div>
                                <input onFocus={ratingFocus} onChange={changeRatingText} className="form-rating" name="rating" type="number" placeholder="Оценка" value={rating.text}/>
                                {rating.errorText && <div className={ratingClasses}>{rating.errorText}</div>}
                            </div>

                        </div>

                        <div className="user">
                            <textarea onChange={changeReviewText} className="form-text" name="text" placeholder="Текст отзыва" value={review}></textarea>
                            <button className="form-button">Отправить отзыв</button>
                        </div>

                    </fieldset>
                </form>

        </div>
    )
}
        
export default Form;
        