import apple from "../../icon/apple.png";

const Footer = () => {
    return (
<footer className="footer">

    <div className="container container_footer">
      <div className="footer-info">
        <div className="footer-info__text"><img className="apple" src={apple} alt="ИконкаЯблочко" /><b>© ООО «<i className="score">Яблочки</i>для вас», 2018-2022.</b></div>
        <div className="footer-info__text">Для уточнения информации звоните по номеру:<a className="link" href="tel:79000000000">+7900000000</a>,</div>
        <div className="footer-info__text">а предложения по сотрудничеству отправляйте на почту:<a className="link" href="mailto:partner@mymarket.com">partner@mymarket.com</a></div>
      </div>
      <a className="link" href="#">Наверх</a>
    </div>

</footer>
    )
}
export default Footer