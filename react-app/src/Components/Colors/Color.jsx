import color1 from "../../images/color-1.webp";
import color2 from "../../images/color-2.webp";
import color3 from "../../images/color-3.webp";
import color4 from "../../images/color-4.webp";
import color5 from "../../images/color-5.webp";
import color6 from "../../images/color-6.webp";

const Colors = () => {
    return (
    <> 
        <h3 className="titles">Цвет товара: синий</h3>
        <div className="color-range">
          <button className="choice-colors">
              <img className="colors" src={color1} alt="Красный" />
          </button>
          <button className="choice-colors">
              <img className="colors" src={color2} alt="Серый" />
          </button>
          <button className="choice-colors">
              <img className="colors" src={color3} alt="Розовый" />
          </button>
          <button className="choice-colors-focus">
              <img className="colors" src={color4} alt="Синий" />
          </button>
          <button className="choice-colors">
              <img className="colors" src={color5} alt="Белый" />
          </button>
          <button className="choice-colors">
              <img className="colors" src={color6} alt="Черный" />
          </button>
        </div>  
    </>
)
}

export default Colors