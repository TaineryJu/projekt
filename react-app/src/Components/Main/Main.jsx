import image1 from "../../images/image-1.webp";
import image2 from "../../images/image-2.webp";
import image3 from "../../images/image-3.webp";
import image4 from "../../images/image-4.webp";
import image5 from "../../images/image-5.webp";

import Colors from "../Colors/Color.jsx";
import Review from "../Reviews/Review.jsx";
import ConfigMemory from "../Configs/Config.jsx";

const Main = () => {
    return (
<main>
    <div className="container">
            <nav className="track">
                <a className="link" href="#">Электроника</a> {">"}
                <a className="link" href="#">Смартфоны и гаджеты</a> {">"}
                <a className="link" href="#">Мобильные телефоны</a> {">"}
                <a className="link" href="#">Apple</a>
            </nav>

          <h2 className="product-name">Смартфон Apple iPhone 13, синий</h2>
            <div className="viewing">
                <div className="view-picture">
                    <img src={image1} alt="Первая картинка" /></div>
                <div className="view-picture">
                    <img src={image2} alt="Вторая картинка" /></div>
                <div className="view-picture">
                    <img src={image3} alt="Третья картинка" /></div>
                <div className="view-picture">
                    <img src={image4} alt="Четвертая картинка" /></div>
                <div className="view-picture">
                    <img src={image5} alt="Пятая картинка" /></div>
            </div>

          <div className="specification">

                <section className="characteristics">

                    <Colors/>

                    <ConfigMemory/>

                    <div className="product-description">
                        <h3 className="titles">Характеристики товара</h3>
                        <ul className="list">
                            <li className="list-item">Экран: <b>6.1</b> </li>
                            <li className="list-item">Встроенная память: <b>128 ГБ</b> </li>
                            <li className="list-item">Встроенная память: <b>128 ГБ</b> </li>
                            <li className="list-item">Операционная система: <b>iOS 15</b> </li>
                            <li className="list-item">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b> </li>
                            <li className="list-item">Процессор: <b><a className="link" href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"> Apple A15 Bionic </a></b></li>
                            <li className="list-item">Вес: <b>173г</b></li>
                        </ul>
                    </div>

                    <div className="description">
                        <h3 className="titles">Описание</h3>
                        Наша самая совершенная система двух камер.
                        <br />Особый взгляд на прочность дисплея.
                        <br />Чип, с которым всё супербыстро.
                        <br />Аккумулятор держится заметно дольше.
                        <br /><i>iPhone 13 - сильный мира всего.</i>
                        <p className="text">Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И повысили скорость работы матрицы на сверхширокоугольной камере.</p>
                        <p className="text">Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                        <p className="text">Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
                    </div>

                    <div className="models">
                        <h3 className="titles">Сравнение моделей</h3>
                          <table className="model">
                            <thead>
                                <tr>
                                    <th className="model__b-right model__indent model__indent">Модель</th>
                                    <th className="model__b-right model__indent model__indent">Вес</th>
                                    <th className="model__b-right model__indent model__indent">Высота</th>
                                    <th className="model__b-right model__indent model__indent">Ширина</th>
                                    <th className="model__b-right model__indent model__indent">Толщина</th>
                                    <th className="model__b-right model__indent model__indent">Чип</th>
                                    <th className="model__b-right model__indent model__indent">Объём памяти</th>
                                    <th className="model__indent">Аккумулятор</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="model__b-top model__b-right model__indent">Iphone11</td>
                                    <td className="model__b-top model__b-right model__indent">194 грамма</td>
                                    <td className="model__b-top model__b-right model__indent">150.9 мм</td>
                                    <td className="model__b-top model__b-right model__indent">75.7 мм</td>
                                    <td className="model__b-top model__b-right model__indent">8.3 мм</td>
                                    <td className="model__b-top model__b-right model__indent">A13 Bionic chip</td>
                                    <td className="model__b-top model__b-right model__indent">До 128 Гб</td>
                                    <td className="model__b-top model__indent">До 17 часов</td>
                                </tr>
                                <tr>
                                    <td className="model__b-right model__indent">Iphone12</td>
                                    <td className="model__b-right model__indent">164 грамма</td>
                                    <td className="model__b-right model__indent">146.7 мм</td>
                                    <td className="model__b-right model__indent">71.5 мм</td>
                                    <td className="model__b-right model__indent">7.4 мм</td>
                                    <td className="model__b-right model__indent">A14 Bionic chip</td>
                                    <td className="model__b-right model__indent">До 256 Гб</td>
                                    <td className="model__indent">До 19 часов</td>
                                </tr>
                                <tr>
                                    <td className="model__b-right model__indent">Iphone13</td>
                                    <td className="model__b-right model__indent">174 грамма</td>
                                    <td className="model__b-right model__indent">146.7 мм</td>
                                    <td className="model__b-right model__indent">71.5 мм</td>
                                    <td className="model__b-right model__indent">7.65 мм</td>
                                    <td className="model__b-right model__indent">A15 Bionic chip</td>
                                    <td className="model__b-right model__indent">До 512 Гб</td>
                                    <td className="model__indent">До 19 часов</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </section>

                  <aside>

                    <div className="important">
                        
                        <div className="price">
                            
                            <div className="discount">
                                <span className="old-price">75 990₽</span>
                                <span className="discount-percentage">-8%</span>
                            </div>
                            
                            <span className="selection">
                                <svg width="50" height="50" viewBox="0 0 50 50" fill="transparent" stroke="#888888" xmlns="http://www.w3.org/2000/svg">
                                    <path className="select-product" d="M23.6152 16.493L25.0002 17.8227L26.3853 16.493L30.6517 12.3973C30.6518 12.3973 30.6518 12.3973 30.6518 12.3972C33.8674 9.3103 39.0916 9.31029 42.3072 12.3972C45.4767 15.44 45.4767 20.3173 42.3073 23.3602C42.3073 23.3602 42.3072 23.3602 42.3072 23.3603L25.0002 39.9748L7.69349 23.3602C7.69348 23.3602 7.69348 23.3602 7.69347 23.3602C4.52387 20.3173 4.5239 15.44 7.69347 12.3972C10.909 9.3103 16.1332 9.31029 19.3488 12.3972C19.3488 12.3972 19.3488 12.3972 19.3488 12.3972L23.6152 16.493Z" strokeWidth="4" />
                                </svg>
                            </span>
                        </div>
                        
                        <h3 className="new-price"> 67 990₽ </h3>
                        
                        <div className="delivery">
                            <p className="text">Самовывоз в четверг, 1 сентября - <b>бесплатно</b></p>
                            <p className="text">Курьером в четверг, 1 сентября - <b>бесплатно</b></p>
                        </div>
                        
                        <div>
                            <button className="add">
                                <div className="add-basket">Добавить в корзину</div>
                            </button>
                        </div>
                    </div>

                      <div className="advertising">
                        <p className="text">Реклама</p>
                        <iframe className="frame" src="./ads.html">Ваш браузер не поддерживает фреймы!</iframe>
                        <iframe className="frame" src="./ads.html">Ваш браузер не поддерживает фреймы!</iframe>
                      </div>

                  </aside>
          </div>

          <Review/>
            
    </div>        
</main>
    )
}

export default Main