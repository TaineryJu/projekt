
import User1 from "../../images/User-1.png";
import User2 from "../../images/User-2.png";

import starActiv from "../../icon/star-activ.png";
import star from "../../icon/star.png";

import Form from "../Form/Form.jsx";

const Review = () => {
    return(
    <>
            <div className="review-section">
                
                <div className="review-title">
                    
                    <div className="text-title">
                        <span className="review">Отзывы</span>
                        <span className="review-visitors">425</span>
                    </div>

                </div>

            </div>

            <div className="review-block">
                <div>
                    <img className="review-image" src={User1} alt="Пользователь" />
                </div>

                <div>
                    <p className="review-text">
                        <b>Марк Г.</b>
                    </p>
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <p className="review-text"><b>Опыт использования:</b> менее месяца </p>
                    <p className="review-text"><b>Достоинства:</b> <br />это мой первый айфон после после огромного количества телефонов на андроиде.всё плавно, чётко и красиво.довольно шустрое устройство.камера весьма неплохая, ширик тоже на высоте.</p>
                    <p className="review-text"><b>Недостатки:</b> <br />к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное.</p>
                </div>

            </div>

            <hr className="separator" />

            <div className="review-block">
                <div>
                    <img className="review-image" src={User2} alt="Пользователь" />
                </div>

                <div>
                    <p className="review-text">
                        <b>Валерий Коваленко</b></p>
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={starActiv} alt="Рэйтинг" />
                    <img className="grade" src={star} alt="Рэйтинг" />
                    <p className="review-text"><b>Опыт использования:</b> менее месяца</p>
                    <p className="review-text"><b>Достоинства:</b> <br />OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго </p>
                    <p className="review-text"><b>Недостатки:</b> <br />Плохая ремонтопригодность</p>
                </div>

            </div>
        <Form />
    </>
)
}

export default Review