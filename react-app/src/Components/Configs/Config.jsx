import "../PageProduct/PageProduct.css";
import {useState} from 'react';

const ConfigMemory = () => {
  const [selected, setSelected] = useState('128 ГБ')

  const changeSelected = (value) => {
    setSelected(value);
  }
    return (

        <div>
            <h3 className="titles">Конфигурация памяти: {selected}</h3>
            <div className="memory-buttons">
                {memoryOptions.map((memory) => {
                    const selectedClass = selected === memory.value ? "memory-selected" : "";
                    return (
                        <button onClick={changeSelected.bind(null, memory.value)} key={memory.id} className={`memory ${selectedClass}`}>
                          {memory.value}
                            </button>
                    )
                })}
            </div>
        </div>
)
}

const memoryOptions = [
    {
      id: '0',
      value: '128 ГБ'
    },
    {
      id: '1',
      value: '256 ГБ'
    },
    {
      id: '2',
      value: '512 ГБ'
    }
  ]
  
export default ConfigMemory