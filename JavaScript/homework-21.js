"use strict";

// Упражнение 1
/**Функция ищет свойства у объекта
 * @param {object} obj - Какой-либо объект проекта
 * @returns {boolean} - Возвращает значение, если объект пустой
 */
 function isEmpty(obj) {
    return Object.keys(obj).length === 0; 
} 

let user = {};
alert(isEmpty(user))


// Упражнение 3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
/** Функция производит повышение зарплат на определенный
процент
 * @param {number} perzent - Значение процента
 * @returns {object} - Возвращает объект с новыми зарплатами
 */
function raiseSalary(perzent) {
    let newSalaries = {};
    for (let key in salaries) {
        let indexing = (salaries[key] * perzent) / 100;
        newSalaries[key] = salaries[key] + indexing;
    }
    return newSalaries;
}
/** Функция суммирует значения свойств объекта
 * @param {object} obj - Объект со значениями
 * @returns {number} - Выводит сумму после изменения
 */
function calcSumm(obj) {
    let summ = 0;
    for (let key in obj) {
        summ += obj[key];
    }
    return summ;
}

let result = raiseSalary(5);
let summ = calcSumm(result);
console.log(result, summ);