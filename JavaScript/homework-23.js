"use strict";

// Упражнение 2
/**Выполнили запрос на бэкенд по адресу: https://reqres.in/api/users
 *  Выводим в консоль количество пользователей и их список: имя, фамилию и e-mail
 */
let promise = fetch("https://reqres.in/api/users"); 
promise
    .then(function (response) {
    return response.json();
})
    .then(function (response) {
    let users = response.data;
        // console.log(users);
    let message = 'Получили пользователей:' + users.length;
        console.log(message);
/**С помощью функции выводим значения по ключю в массиве
 * @param {Array} user - Массив пользователей
 */
    users.forEach(function(user) {
    let newMessage = '-' + user.first_name +' '+ user.last_name;
    newMessage += '('+ user.email +')';
        console.log(newMessage);
    })
    })

// // Упражнение 1
/** Запрашиваем у пользователя число и запускаем обратный отсчет
 * @param {number} num - вводимое пользователем число
 * @returns {number} - обратный отсчет до 0
 */
let num = +prompt("Введите число"); 
    let intervalId = setInterval(function () {
    num = num - 1
    console.log("Осталось", num)
    if (num === 0) {
        clearInterval(intervalId);
        console.log("Время вышло")
        }
        else if (isNaN(num)) {
        alert('Вы ввели не число');
        num = +prompt("Введите число");
    }
}, 1000);