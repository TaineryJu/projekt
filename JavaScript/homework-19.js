"use strict";

// Упражнение 1
let a = '$100';
let b = '300$';
// Решение
let summ = parseInt(a.slice(1)) + parseInt(b);
console.log(summ);

// Упражнение 2
let message = ' привет, медвед ';
// Решение
message = message.trim().charAt(0).toUpperCase() + message.trim().slice(1);
console.log(message);

// Упражнение 3
let age = prompt('Сколько вам лет?');
// Решение
if (age <=3) 
{alert(`Вам ${age} лет и вы младенец`);}
else if (age <=11) 
{alert(`Вам ${age} лет и вы ребенок`);} 
else if (age <=18) 
{alert(`Вам ${age} лет и вы подросток`);} 
else if (age <=40) 
{alert(`Вам ${age} лет и вы познаёте жизнь`);}
else if (age <=80) 
{alert(`Вам ${age} лет и вы познали жизнь`);} 
else if (age >=81) 
{alert(`Вам ${age} лет и вы долгожитель`);}
