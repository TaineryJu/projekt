const formUser = document.querySelector(".form-user")
const formRating = document.querySelector(".form-rating")
const formText = document.querySelector(".form-text")
    
const faultFormUser = document.querySelector(".form-user-fault")
    faultFormUser.style.display = "none";
const faultFormRating = document.querySelector(".form-rating-fault")
    faultFormRating.style.display = "none";

const add = document.querySelector(".add")
const addBasket = document.querySelector(".add-basket")
const yourBasket = document.querySelector(".your_basket")
    yourBasket.style.display = "none";

const selection = document.querySelector(".selection")
const selectProduct = document.querySelector(".select-product")
const favorites = document.querySelector(".favorites")
const yourSelect = document.querySelector(".your_select")
    yourSelect.style.display = "none";

const form = document.querySelector("form");

form.addEventListener("submit", formValidation);

function formValidation(event) {
    event.preventDefault();
let user = formUser.value; 
    if (user.length == 0) {
        formUser.style.border = "1px solid #ff0000";
        faultFormUser.textContent = "Вы забыли указать имя и фамилию";
        faultFormUser.style.display = "";
        return;
}
    else if (user.length == 1) {
        formUser.style.border = "1px solid #ff0000";
        faultFormUser.textContent = "Имя не может быть короче 2-х символов";
        faultFormUser.style.display = "";
        return;
}
    else if (user.length >= 2) {
        formUser.style.border = "";
        faultFormUser.style.display = "none";
};

let rating = formRating.value;
    if (rating < 1 || rating > 5) {
        faultFormRating.textContent = "Оценка должна быть от 1 до 5";
        formRating.style.border = "1px solid #ff0000";
        faultFormRating.style.display = "";
        return;
}
    else { formRating.style.border = "";
            faultFormRating.style.display = "none";
};
    localStorage.removeItem("user");
    localStorage.removeItem("rating");
    localStorage.removeItem("text");
    formUser.value = "";
    formRating.value = "";
    formText.value = "";
};

form.addEventListener("input", function () {
    faultFormUser.style.display = "none";
    formUser.style.border = "";
    faultFormRating.style.display = "none";
    formRating.style.border = "";
});

formUser.value = localStorage.getItem("user") ?? "";
formRating.value = localStorage.getItem("rating") ?? "";
formText.value = localStorage.getItem("text") ?? "";

form.addEventListener("input", saveForm);
function saveForm(event) {
    const element = event.target;
    const name = element.getAttribute("name");
    const value = element.value;
    console.log(element, name, value);
    localStorage.setItem(name, value);
};

addBasket.addEventListener("click", addToBasket);
let basket = JSON.parse(localStorage.getItem("yourBasket")) ?? false;
    if (basket) {
    yourBasket.textContent = "1";
    add.style.background = "#888888";
    addBasket.textContent = "Товар уже в корзине";
    yourBasket.style.display ="";
}
function addToBasket() {
    if (!basket) {
    yourBasket.textContent = "1";
    add.style.background = "#888888";
    addBasket.textContent = "Товар уже в корзине";
    yourBasket.style.display ="";
    localStorage.setItem("yourBasket", true);
    basket = true;
    }
    else { 
        add.style.background = "";
        yourBasket.style.display = "none";
        addBasket.textContent = "Добавить в корзину";
        localStorage.setItem("yourBasket", false);
        basket = false;
}}

selection.addEventListener("click", addToSelect);
let select = JSON.parse(localStorage.getItem("yourSelect")) ?? false;
if (select) {
    yourSelect.textContent = "1";
    yourSelect.style.display = "";
    selectProduct.setAttribute("stroke", "#F36223");
    selectProduct.setAttribute("stroke-width", "4");
    selectProduct.setAttribute("fill", "#F36223");
}
function addToSelect() {
    if (!select) {
    yourSelect.textContent = "1";
    yourSelect.style.display = "";
    selectProduct.setAttribute("stroke", "#F36223");
    selectProduct.setAttribute("stroke-width", "4");
    selectProduct.setAttribute("fill", "#F36223");
    localStorage.setItem("yourSelect", true);
    select = true;
} else {
    yourSelect.style.display = "none";
    selectProduct.setAttribute("stroke", "#888");
    selectProduct.setAttribute("stroke-width", "4");
    selectProduct.setAttribute("fill", "#888");
    localStorage.setItem("yourSelect", false);
    select = false;
}}