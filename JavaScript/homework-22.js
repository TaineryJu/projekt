"use strict";

// Упражнение 1
/** Функция считает сумму чисел массива, исключая другие значения
 * @param {array} arr - значения элементов массива
 * @returns {number} - сумма чисел
  */  
function getSumm(arr) {
  let newArr = arr.filter(Number);
  let sum = 0
  for (let n = 0; n < newArr.length; n++) {
    sum += newArr[n]
    }
    console.log(sum);
}

let arr1 = [1, 2, 10, 5]
getSumm(arr1);

let arr2 = ["a", {}, 3, 3, -2]
getSumm(arr2);




// Упражнение 3
let cart = [4884];
/**Функция добавляет индекс товара в массив корзины
 * @param {number} id - индекс товара
 * @returns {array<number>} - новый массив товаров в корзине
 */
function addToCart(id) {
  if (cart.includes(id)) {
    cart.splice(id);
  } else {
    cart.push(id);
}
}

addToCart(4884)
addToCart(3456)
console.log(cart);

/** Функция удаляет из массива товаров заданный индекс
 * @param {number} id - индекс товара
 * @returns {array<number>} - новый массив товаров в корзине
 */
function removeFromCart(id) {
  cart.splice(cart[id], 1);
 };

removeFromCart(4884)
console.log(cart);
