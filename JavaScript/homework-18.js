"use strict";

// Упражнение 1
let a = '100px';
let b = '323px';
let result = (parseInt(a) + parseInt(b));
// Решение
console.log(result);

// Упражнение 2
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

// Упражнение 3
let d = 0.111; 
// Решение
console.log(Math.ceil(d));

// Упражнение 4
let e = 45.333333;
// Решение
console.log(e.toFixed(1));

// Упражнение 5
let f = 3;
// Решение
console.log(((f)**5));

// Упражнение 6
let g = 400000000000000;
// Решение
console.log((4e14));

// Упражнение 7
let h = '1' != 1;
// Решение
console.log(('1' == 1));